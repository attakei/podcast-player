import * as React from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'


interface Dispathable {
  handleDispatch: Function,
}

interface IState {
  url: string
}


class Form extends React.Component<Dispathable, IState> {
  constructor(props: Dispathable) {
    super(props)
    this.state = {
      url: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  handleChange(event: any) {
    this.setState({ url: event.target.value })
  }

  onSubmit(event: any) {
    this.props.handleDispatch(this.state.url)
      .then(() => {
        this.setState({ url: '' })
      })
    event.preventDefault()
  }

  render() {
    const flexContainer = {
      display: 'flex',
      flexDirection: 'row',
    };
    
    return (
      // TODO: Fix?
      <form style={flexContainer} onSubmit={this.onSubmit}>
        <TextField
            fullWidth
            id="standard-name"
            label="URL"
            value={this.state.url}
            onChange={this.handleChange}
            margin="normal"
          />
        <Button variant="contained" color="inherit" type="submit">
          取得
        </Button>
      </form>
    )
  }
}


export default Form

import store from 'store2'
import React from 'react';
import { Grid } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles';
import Image from 'material-ui-image'
import Parser from 'rss-parser'
import MediaPlayer from '../src/media-player/MediaPlayer';
import Playlist from '../src/media-player/Playlist';
import Form from '../src/podcast-form';


const styles = (theme: any) => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 20,
  },
});

interface PodcastMedia {
  title: string,
  source: string | null,
  link: string | null,
  thumbnail: string | null,
}

interface PlaylistState {
  open: boolean,
  currentTrack: PodcastMedia,
  autoPlay: boolean,
  repeatTrack: boolean,
  playlist: Array<PodcastMedia>,
}

class Index extends React.Component<any, PlaylistState> {
  constructor(props: any) {
    super(props)
    this.dispatchPodcast = this.dispatchPodcast.bind(this)
    let playlist: any|null = store.get('playlist')
    if (!playlist) {
      playlist = new Array<PodcastMedia>()
    }
    this.state = {
      open: false,
      currentTrack: { source: null, title: 'No media loaded', link: null, thumbnail: null },
      autoPlay: true,
      repeatTrack: true,
      playlist: playlist,
    }
  }

  componentDidUpdate(prevProps: any, prevState: PlaylistState, snapshot: any) {
    store.set('playlist', prevState.playlist)
  }


  _navigatePlaylist = (direction: number) => {
    const mod = (num: number, max: number): number => ((num % max) + max) % max
    const newIndex = mod(
      this.state.playlist.indexOf(this.state.currentTrack) + direction,
      this.state.playlist.length
    )
    this.setState({ currentTrack: this.state.playlist[newIndex] })
  }
  
  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  handleClick = () => {
    this.setState({
      open: true,
    });
  };

  _handleTrackClick = (track: PodcastMedia) => {
    this.setState({ currentTrack: track })
  }

  dispatchPodcast(url: string) {
    const parser: any = new Parser()
    const proxyedUrl = `/api/proxy/podcast?url=${url}`
    return parser.parseURL(proxyedUrl)
      .then((feed: any) => {
        let thumbnail: string | null
        if (feed.itunes) {
          thumbnail = feed.itunes.image.href
        } else if(feed.image) {
          thumbnail = feed.image.url
        } else {
          thumbnail = null
        }
        feed.items.forEach((item: any) => {
          this.setState((state: any) => {
            const beforePlaylistLength = state.playlist.length
            state.playlist.push({
              title: item.title,
              source: item.enclosure.url,
              page: item.link,
              description: item.description,
              thumbnail: thumbnail,
            })
            if (beforePlaylistLength === 0) {
              this._navigatePlaylist(1)
            }
            return state
          })
        })
      })


  }

  render() {
    const { currentTrack, autoPlay, repeatTrack, playlist } = this.state;
    return (
      <Grid container align="center" alignContent="center" justify="center" spacing={24}>
        <Grid item xs={12} sm={12} >
          <h1>Podcast player</h1>
          <div className="media-player-wrapper">
            <div>
              <Form handleDispatch={this.dispatchPodcast} />
              <p>このフォームにPodcastのURLを入れてEnterを押すと、<br />プレイリストに追加されます</p>
            </div>
            {currentTrack.thumbnail && <Image src={currentTrack.thumbnail} />}
            <MediaPlayer
              // ref={c => (this._mediaPlayer = c)}
              src={currentTrack.source}
              autoPlay={autoPlay}
              loop={repeatTrack}
              currentTrack={currentTrack.title}
              repeatTrack={repeatTrack}
              onPrevTrack={() => this._navigatePlaylist(-1)}
              onNextTrack={() => this._navigatePlaylist(1)}
              onRepeatTrack={() => {
                this.setState({ repeatTrack: !repeatTrack })
              }}
              onPlay={() => !autoPlay && this.setState({ autoPlay: true })}
              onPause={() => this.setState({ autoPlay: false })}
              onEnded={() => !repeatTrack && this._navigatePlaylist(1)}
            />
            <Playlist
              tracks={playlist}
              currentTrack={currentTrack}
              onTrackClick={this._handleTrackClick}
            />
          </div>
        </Grid>
      </Grid>
    )
  }
}


export default withStyles(styles)(Index)

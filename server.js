// This file doesn't go through babel or webpack transformation.
// Make sure the syntax and sources this file requires are compatible with the current node version you are running
// See https://github.com/zeit/next.js/issues/1245 for discussions on Universal Webpack or universal Babel
const { createServer } = require('http')
const { parse } = require('url')
const next = require('next')
const request = require('request')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const proxyPodcast = (req, res) => {
  const parsedUrl = parse(req.url, true)
  const target = parsedUrl.query.url
  if (!target) {
    res.write(JSON.stringify({msg: 'invalid url'}))
    res.end()
    return
  }
  request(target, (error, response, body) => {
    if (error) {
      res.write(JSON.stringify({msg: error.msg}))
      res.end()
    }
    res.write(body)
    res.end()
      
  })
}


app.prepare().then(() => {
  const server = createServer((req, res) => {
    // Be sure to pass `true` as the second argument to `url.parse`.
    // This tells it to parse the query portion of the URL.
    const parsedUrl = parse(req.url, true)
    const { pathname, query } = parsedUrl

    if (pathname.startsWith('/api/proxy/podcast')) {
      proxyPodcast(req, res)
    } else {
      handle(req, res, parsedUrl)
    }
  }).listen(process.env.PORT || 3000, err => {
    if (err) throw err
    console.log('> Ready on http://localhost:3000')
  })
})
